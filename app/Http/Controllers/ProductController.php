<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

	/**
	/* Product form.
	*/
    public function create(){

    	return view('product.create');
    }


    public function store(Request $request) {

       
        $product = new Product;
        $product->name = $request->name;
        $product->amount = $request->amount;

         // NORMAL WAY, SAVING DATA IN OUT MODEL TABLE.
        if(!$product->save()){
        	\Session::flash('flash_message', 'Error....'); 
        	return redirect('');
        }
        

        // Save data in a json file with storage
        $productJson = $product->toJson();

        if (Storage::put('product.json', $productJson)){
            \Session::flash('flash_message', 'New product has been added.'); //<--FLASH MESSAGE
   
        }
        return redirect('product');
    }

}
