@extends('layouts.app')

@section('content')
{!! Form::model($product, ['method' => 'POST', 'action' => ['ProductController@store']]) !!}

 @include('partials._test_form', ['submitButton' => 'Save product'])

{!! Form::close() !!}


@endsection

@section('footer')
	Product footer!.

@endsection
