<!-- name Form Input use this code in edit and  create forms --> 
<div class="form-group">
    {!! Form::label('Product name', 'Product name :') !!}
    {!! Form::text('productName', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    {!! Form::label('Product name', 'Name :') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('Quantity stock', 'Quantity stock :') !!}
    {!! Form::text('qStock', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('Price per item', 'Price :') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
 {!! Form::submit($submitButton) !!}
</div>